
from flask import Flask
from flask import render_template
from flask import request
import webview
import sys
import threading
import json
import os
import fillpdf_built

with open('labs.json') as n:
    LABS = json.load(n)
 
app = Flask(__name__)
def start_server():
    app.run(host='0.0.0.0', port=80)

@app.route('/')

def lab_selection_page():
    
    f = [{
        "type":"radio",
        "field_name":"lab",
        "display_name":"Select Your Lab",
        "display_value_dict":[[i,o['display']] for i , o in LABS['labs'].items()]
    }]

    return render_template('index.html',fields = f,url ='/lab')

@app.route('/lab')
def form_filler_page():
    x = request.args['lab']
    return render_template('index.html',fields = LABS['labs'][x]['fields'],test=x,url='/makepdf')

@app.route('/makepdf')
def create_pdf():
    args = dict(request.args)
    print(args)
    x = {'full_name': '', 'dob_mm/dd/yyyy': '10/11/1999', 'gender': 'Male', 'ses_id': 'dfsfsdfsdfsdfsdfsdf', 'start_time': 'fsdfsdf', 'end_time': 'sdfsdf', 'dot': '02/02/0222'}
    return 'Done'


if __name__ == '__main__':
    t = threading.Thread(target=start_server)
    t.daemon = True
    t.start()
    webview.create_window("Test Result Generator", "http://localhost/",width=400, height=600,resizable=False)
    webview.start()
    sys.exit()